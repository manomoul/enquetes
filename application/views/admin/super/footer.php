<?php
/**
 * Footer view
 * Inserted in all pages
 */
?>

<!-- Footer -->
<footer class='footer'>
	<div class="container-fluid">
		<div class="row">
		    <!-- Link to manual -->
		    <div class="col-xs-6 col-md-4 col-lg-1 ">
		    	<a href='http://manual.limesurvey.org'>
                    <span class="glyphicon glyphicon-info-sign" id="info-footer"></span>
                </a>
		    </div>

		    

		    <!-- Lime survey website -->
		    <div class="col-xs-6 col-md-4 col-lg-6 text-right pull-right">
		    	<a  title='<?php eT("Visit our website!"); ?>' href='http://www.limesurvey.org' target='_blank'>LimeSurvey</a><br /><?php echo $versiontitle."  ".$versionnumber." ".$buildtext;?>
		    </div>
		</div>
	</div>
</footer>


</body>
</html>
